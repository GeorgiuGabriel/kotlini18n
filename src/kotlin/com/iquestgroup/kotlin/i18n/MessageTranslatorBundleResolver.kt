package com.iquestgroup.kotlin.i18n

import java.util.*

class MessageTranslatorBundleResolver {

    fun getResourceBundle(language: String?, bundleName: String): ResourceBundle {
        Locale.setDefault(getLocale(language))
        return ResourceBundle.getBundle(bundleName)
    }

    private fun getLocale(language: String?): Locale {
        if (language == null || language.isEmpty()) {
            return Locale.getDefault()
        }

        return Locale(language)
    }
}