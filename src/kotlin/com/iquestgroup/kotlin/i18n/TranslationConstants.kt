package com.iquestgroup.kotlin.i18n

object TranslationConstants {
   const val BUNDLE_NAME = "Messages"
   const val GREETING_MESSAGE = "greeting"
}