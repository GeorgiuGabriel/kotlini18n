package com.iquestgroup.kotlin

import com.iquestgroup.kotlin.i18n.TranslationConstants.BUNDLE_NAME
import com.iquestgroup.kotlin.i18n.MessageTranslatorBundleResolver
import com.iquestgroup.kotlin.i18n.TranslationConstants.GREETING_MESSAGE

import javax.jws.WebMethod
import javax.jws.WebParam
import javax.jws.WebService
import javax.xml.ws.Endpoint

@WebService
class InternationalHello {

    @WebMethod
    open fun translatedHello(@WebParam language: String?): String {
        val messageTranslatorBundleResolver = MessageTranslatorBundleResolver()
        val resourceBundle = messageTranslatorBundleResolver.getResourceBundle(language, BUNDLE_NAME)

        return resourceBundle.getString(GREETING_MESSAGE)
    }

}

fun main(args: Array<String>) {
    Endpoint.publish("http://localhost:8081/hello", InternationalHello())
}
